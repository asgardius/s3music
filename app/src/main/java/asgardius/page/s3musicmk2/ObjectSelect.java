package asgardius.page.s3musicmk2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ObjectSelect extends AppCompatActivity {

    ArrayList Name;
    ArrayList Img;
    //ArrayList object;
    RecyclerView recyclerView;
    String username, password, endpoint, bucket, prefix, location, policy;
    boolean style, isplaylist;
    String[] filename;
    Region region;
    S3ClientOptions s3ClientOptions;
    AWSCredentials myCredentials;
    AmazonS3 s3client;
    ProgressBar simpleProgressBar;
    int videocache, videotime, buffersize, treelevel, playlisttime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        endpoint = getIntent().getStringExtra("endpoint");
        username = getIntent().getStringExtra("username");
        password = getIntent().getStringExtra("password");
        bucket = getIntent().getStringExtra("bucket");
        location = getIntent().getStringExtra("region");
        style = getIntent().getBooleanExtra("style", false);
        prefix = getIntent().getStringExtra("prefix");
        treelevel = getIntent().getIntExtra("treelevel", 0);
        videocache = getIntent().getIntExtra("videocache", 40);
        videotime = getIntent().getIntExtra("videotime", 1);
        playlisttime = getIntent().getIntExtra("playlisttime", 1);
        buffersize = getIntent().getIntExtra("buffersize", 2000);
        isplaylist = getIntent().getBooleanExtra("isplaylist", false);
        setContentView(R.layout.activity_object_select);
        getSupportActionBar().setTitle(bucket+"/"+prefix);
        region = Region.getRegion(location);
        s3ClientOptions = S3ClientOptions.builder().build();
        myCredentials = new BasicAWSCredentials(username, password);
        try {
            s3client = new AmazonS3Client(myCredentials, region);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),e.toString(), Toast.LENGTH_SHORT).show();
        }
        s3client.setEndpoint(endpoint);
        s3ClientOptions.setPathStyleAccess(style);

        s3client.setS3ClientOptions(s3ClientOptions);


        recyclerView = findViewById(R.id.olist);
        simpleProgressBar = (ProgressBar) findViewById(R.id.simpleProgressBar);

        // layout for vertical orientation
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        Thread listobject = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                    //Your code goes here
                    //List<Bucket> buckets = s3client.listBuckets();
                    ListObjectsRequest orequest;
                    if (treelevel == 0) {
                        orequest = new ListObjectsRequest().withBucketName(bucket).withMaxKeys(1000).withDelimiter("/");
                    } else {
                        orequest = new ListObjectsRequest().withBucketName(bucket).withPrefix(prefix).withMaxKeys(1000).withDelimiter("/");
                    }
                    //List<S3Object> objects = (List<S3Object>) s3client.listObjects(bucket, "/");
                    ObjectListing result = s3client.listObjects(orequest);
                    //System.out.println(objects);
                    //This convert bucket list to an array list
                    Img = new ArrayList<String>();
                    ArrayList<String> object = new ArrayList<String>();
                    // Print bucket names
                    //System.out.println("Buckets:");
                    //int i=0;
                    //This get folder list
                    List<String> od = result.getCommonPrefixes();
                    for (String os : od) {
                        filename = os.split("/");
                        if (filename.length == treelevel+1) {
                            object.add(filename[treelevel]+"/");
                        }

                        //i++;
                    }
                    //This get file list
                    List<S3ObjectSummary> ob = result.getObjectSummaries();
                    for (S3ObjectSummary os : ob) {
                        filename = os.getKey().split("/");
                        if (filename.length == treelevel+1) {
                            object.add(filename[treelevel]);
                        }
                        else {
                            object.add(filename[treelevel]+"/");
                        }

                        //i++;
                    }

                    //Get next batch
                    while (result.isTruncated()) {
                        result = s3client.listNextBatchOfObjects (result);

                        //This get folder list
                        od = result.getCommonPrefixes();
                        for (String os : od) {
                            filename = os.split("/");
                            if (filename.length == treelevel+1) {
                                object.add(filename[treelevel]+"/");
                            }

                            //i++;
                        }

                        //This get file list
                        ob = result.getObjectSummaries();
                        for (S3ObjectSummary os : ob) {
                            filename = os.getKey().split("/");
                            if (filename.length == treelevel+1) {
                                object.add(filename[treelevel]);
                            }
                            else {
                                object.add(filename[treelevel]+"/");
                            }

                            //i++;
                        }

                    }

                    Name = new ArrayList<String>(object);
                    object.clear();
                    //Img.add(R.drawable.unknownfile);
                    //This set object icon based on its filetype
                    int i = 0;
                    while(i<Name.size()) {
                        //Img.add(R.drawable.unknownfile);
                        if (Name.get(i).toString().endsWith("/")) {
                            Img.add(R.drawable.folder);
                        }
                        else if (Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".txt") || Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".md")) {
                            Img.add(R.drawable.ptextfile);
                        }
                        else if (Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".pdf")) {
                            Img.add(R.drawable.pdffile);
                        }
                        else if (Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".jpg") || Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".jpeg")
                                || Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".png") || Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".gif")
                                || Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".webp")) {
                            Img.add(R.drawable.imagefile);
                        }
                        else if (Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".opus") || Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".ogg")
                                || Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".oga") || Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".mp3")
                                || Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".m4a") || Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".flac")
                                || Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".mka") || Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".m3u")) {
                            Img.add(R.drawable.audiofile);
                        }
                        else if(Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".mp4") || Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".mkv")
                                || Name.get(i).toString().endsWith(".webm") || Name.get(i).toString().endsWith(".m4v") || Name.get(i).toString().endsWith(".m3u8")) {
                            Img.add(R.drawable.videofile);
                        }
                        else if (Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".htm") || Name.get(i).toString().toLowerCase(Locale.ROOT).endsWith(".html")) {
                            Img.add(R.drawable.webpage);
                        }
                        else {
                            Img.add(R.drawable.unknownfile);
                        }
                        i++;
                    }

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // Sending reference and data to Adapter
                            Adapter adapter = new Adapter(ObjectSelect.this, Img, Name);
                            simpleProgressBar.setVisibility(View.INVISIBLE);

                            // Setting Adapter to RecyclerView
                            recyclerView.setAdapter(adapter);
                        }
                    });
                    //System.out.println("tree "+treelevel);
                    //System.out.println("prefix "+prefix);

                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    //Toast.makeText(getApplicationContext(),e.toString(), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
        listobject.start();
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //System.out.println("Click on "+Name.get(position).toString());
                //explorer(Name.get(position).toString());
                if (Img.get(position).equals(R.drawable.folder)) {
                    //go to subfolder
                    explorer(Name.get(position).toString());
                } else if (Img.get(position).equals(R.drawable.audiofile) || Img.get(position).equals(R.drawable.videofile)) {
                    simpleProgressBar.setVisibility(View.VISIBLE);
                    if (isplaylist && !Name.get(position).toString().toLowerCase(Locale.ROOT).endsWith(".m3u")) {
                        videoPlayer(null, Name.get(position).toString());
                    } else {
                        Thread mediaread = new Thread(new Runnable() {

                            @Override
                            public void run() {
                                try  {
                                    //load media file
                                    Date expiration = new Date();
                                    Calendar mycal = Calendar.getInstance();
                                    mycal.setTime(expiration);
                                    //System.out.println("today is " + mycal.getTime());
                                    mycal.add(Calendar.HOUR, videotime);
                                    //System.out.println("Expiration date: " + mycal.getTime());
                                    expiration = mycal.getTime();
                                    try {
                                        policy = s3client.getBucketPolicy(bucket).getPolicyText();
                                    } catch (Exception e) {
                                        policy = null;
                                    }
                                    String objectURL = PolicyCheck.getFileKey(s3client, bucket, prefix + Name.get(position).toString(), expiration, policy);
                                    /*GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(bucket, prefix + Name.get(position).toString()).withExpiration(expiration);;
                                    URL objectURL = s3client.generatePresignedUrl(request);*/

                                    runOnUiThread(new Runnable() {

                                        @Override
                                        public void run() {
                                            // Sending reference and data to Adapter
                                            videoPlayer(objectURL, Name.get(position).toString());
                                        }
                                    });
                                    //System.out.println("tree "+treelevel);
                                    //System.out.println("prefix "+prefix);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    runOnUiThread(new Runnable() {

                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplicationContext(),e.toString(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                    //Toast.makeText(getApplicationContext(),e.toString(), Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            }
                        });
                        mediaread.start();
                    }
                }  else {
                    Toast.makeText(ObjectSelect.this, getResources().getString(R.string.unsupported_file), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                //System.out.println("Long click on "+Name.get(position).toString());
            }
        }));
    }

    private void videoPlayer(String url, String title) {
        if (title.toLowerCase(Locale.ROOT).endsWith(".m3u")) {
            Thread mediaread = new Thread(new Runnable() {

                @Override
                public void run() {
                    try  {
                        //load media file
                        ArrayList<String> links = getPlaylist(url);
                        ArrayList<String> medialist = new ArrayList<String>();
                        for (int i = 0; i < links.size(); i++) {
                            medialist.add(links.get(i).toString());
                        }

                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                // Sending reference and data to Adapter
                                //videoPlayer(objectURL.toString(), Name.get(position).toString());
                                Intent intent = new Intent(getApplicationContext(), VideoPlayer.class);
                                intent.putExtra("video_url", url);
                                intent.putExtra("title", title);
                                intent.putExtra("videocache", videocache);
                                intent.putExtra("buffersize", buffersize);
                                intent.putExtra("isplaylist", true);
                                intent.putExtra("queue", links);
                                intent.putExtra("names", medialist);
                                simpleProgressBar.setVisibility(View.INVISIBLE);
                                startActivity(intent);
                            }
                        });
                        //System.out.println("tree "+treelevel);
                        //System.out.println("prefix "+prefix);

                    } catch (Exception e) {
                        e.printStackTrace();
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            });
            mediaread.start();
        }
        else if (isplaylist) {
            Thread mediaread = new Thread(new Runnable() {

                @Override
                public void run() {
                    try  {
                        //load media file
                        ArrayList<String> medialist = new ArrayList<String>();
                        for (int i = 0; i < Name.size(); i++) {
                            if (Img.get(i).equals(R.drawable.audiofile) || Img.get(i).equals(R.drawable.videofile)) {
                                medialist.add(Name.get(i).toString());
                            }
                        }
                        ArrayList<String> links = getLinks(medialist);

                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                if(links != null) {
                                    // Sending reference and data to Adapter
                                    //videoPlayer(objectURL.toString(), Name.get(position).toString());
                                    Intent intent = new Intent(getApplicationContext(), VideoPlayer.class);
                                    intent.putExtra("video_url", url);
                                    intent.putExtra("title", title);
                                    intent.putExtra("videocache", videocache);
                                    intent.putExtra("buffersize", buffersize);
                                    intent.putExtra("isplaylist", isplaylist);
                                    intent.putExtra("queue", links);
                                    intent.putExtra("names", medialist);
                                    simpleProgressBar.setVisibility(View.INVISIBLE);
                                    startActivity(intent);
                                }
                            }
                        });
                        //System.out.println("tree "+treelevel);
                        //System.out.println("prefix "+prefix);

                    } catch (Exception e) {
                        e.printStackTrace();
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            });
            mediaread.start();
        } else {
            Intent intent = new Intent(this, VideoPlayer.class);
            intent.putExtra("video_url", url);
            intent.putExtra("title", title);
            intent.putExtra("videocache", videocache);
            intent.putExtra("buffersize", buffersize);
            intent.putExtra("isplaylist", isplaylist);
            simpleProgressBar.setVisibility(View.INVISIBLE);
            startActivity(intent);
        }

    }

    private void explorer(String object) {

        Intent intent = new Intent(this, ObjectSelect.class);
        //treelevel ++;
        intent.putExtra("endpoint", endpoint);
        intent.putExtra("username", username);
        intent.putExtra("password", password);
        intent.putExtra("bucket", bucket);
        intent.putExtra("prefix", prefix + object);
        intent.putExtra("treelevel", treelevel+1);
        intent.putExtra("region", location);
        intent.putExtra("style", style);
        intent.putExtra("videocache", videocache);
        intent.putExtra("videotime", videotime);
        intent.putExtra("buffersize", buffersize);
        intent.putExtra("playlisttime", playlisttime);
        intent.putExtra("isplaylist", isplaylist);
        startActivity(intent);

    }
    public ArrayList<String> getLinks (ArrayList<String> medialist) throws InterruptedException {
        ArrayList<String> links = new ArrayList<String>();
        Date expiration = new Date();
        Calendar mycal = Calendar.getInstance();
        mycal.setTime(expiration);
        //System.out.println("today is " + mycal.getTime());
        mycal.add(Calendar.HOUR, playlisttime);
        //System.out.println("Expiration date: " + mycal.getTime());
        expiration = mycal.getTime();
        for (int i = 0; i < medialist.size(); i++) {
            //GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(bucket, prefix+medialist.get(i)).withExpiration(expiration);;
            //links.add(s3client.generatePresignedUrl(request).toString());
            links.add(PolicyCheck.getFileKey(s3client, bucket, prefix+medialist.get(i), expiration, policy));
        }
        return links;
    }
    public ArrayList<String> getPlaylist(String playlist) {
        ArrayList<String> links = new ArrayList<String>();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader((new URL(playlist).openStream())));
            String strLine;

//Read File Line By Line
            while ((strLine = br.readLine()) != null)   {
                // Print the content on the console - do what you want to do
                if(!strLine.startsWith("#")) {
                    links.add(strLine);
                }
            }

//Close the input stream
            return links;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}