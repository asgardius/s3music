package asgardius.page.s3musicmk2;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;

import java.util.Date;

public class PolicyCheck {
    public static String getFileKey(AmazonS3 s3client, String bucket, String object, Date expiration, String policy) {
        try {
            Boolean publicobject;
            String fileKey = null;
            publicobject = false;
            if(policy.contains("arn:aws:s3:::"+bucket+"/*") && policy.contains("s3:GetObject")) {
                publicobject = true;
            } else if(policy.contains("s3:GetObject")) {
                if((policy.contains("\"arn:aws:s3:::"+bucket+"/"+object+"\"") || policy.contains("\"arn:aws:s3:::"+bucket+"/"+object+"*\"") || policy.contains("\"arn:aws:s3:::"+bucket+"/"+object+"**\"")) && policy.contains("s3:GetObject")) {
                    publicobject = true;
                } else {
                    String[] path = object.split("/");
                    String filepath = "";
                    for (int i = 0; i < path.length-1; i++) {
                        filepath = filepath+path[i]+"/";
                        //System.out.println(filepath);
                        if(policy.contains("\"arn:aws:s3:::"+bucket+"/"+filepath+"*\"") || policy.contains("\"arn:aws:s3:::"+bucket+"/"+filepath+"**\"")) {
                            publicobject = true;
                            i = path.length;
                        }
                    }
                }
            }
            if(publicobject) {
                fileKey = s3client.getUrl(bucket, object).toString();
            } else {
                GeneratePresignedUrlRequest request;
                request = new GeneratePresignedUrlRequest(bucket, object).withExpiration(expiration);
                fileKey = s3client.generatePresignedUrl(request).toString();
            }
            return fileKey;
        } catch (Exception e) {
            GeneratePresignedUrlRequest request;
            request = new GeneratePresignedUrlRequest(bucket, object).withExpiration(expiration);
            return s3client.generatePresignedUrl(request).toString();
        }
    }
}
