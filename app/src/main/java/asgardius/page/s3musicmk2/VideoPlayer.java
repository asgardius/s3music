package asgardius.page.s3musicmk2;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.media.session.MediaSessionCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.MediaMetadata;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Tracks;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.database.StandaloneDatabaseProvider;
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;
import com.google.android.exoplayer2.ui.StyledPlayerControlView;
import com.google.android.exoplayer2.ui.StyledPlayerView;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class VideoPlayer extends AppCompatActivity {

    // creating a variable for exoplayerview.
    protected StyledPlayerView playerView;
    protected StyledPlayerControlView controlView;
    private WifiManager.WifiLock mWifiLock;
    private PowerManager.WakeLock mWakeLock;
    private PowerManager powerManager;
    private long maxCacheSize;
    private TextView tn,an;
    ImageView previous, playpause, next;
    ArrayList<String> queue, names;
    LeastRecentlyUsedCacheEvictor evictor;
    StandaloneDatabaseProvider standaloneDatabaseProvider;
    SimpleCache simpleCache;
    Drawable play, pause;
    int videocache, buffersize;
    ProgressiveMediaSource mediaSource;
    DefaultLoadControl loadControl;
    DefaultRenderersFactory renderersFactory;
    ExoPlayer player;
    long videoPosition;
    MediaSessionCompat mediaSession;
    MediaSessionConnector mediaSessionConnector;
    private PlayerNotificationManager playerNotificationManager;
    private int notificationId = 1234;
    boolean isplaylist;
    boolean success = false;
    String videoURL, title, artistname, trackname;

    public static String URLify(String str) {
        str = str.trim();
        int length = str.length();
        int trueL = length;
        if(str.contains(" ")) {
            for(int i = 0; i < length; i++) {
                if(str.charAt(i) == ' ') {
                    trueL = trueL + 2;
                }
            }
            char[] oldArr = str.toCharArray();
            char[] newArr = new char[trueL];
            int x = 0;
            for(int i = 0; i < length; i++) {
                if(oldArr[i] == ' ') {
                    newArr[x] = '%';
                    newArr[x+1] = '2';
                    newArr[x+2] = '0';
                    x += 3;
                } else {
                    newArr[x] = oldArr[i];
                    x++;
                }
            }
            str = new String(newArr, 0, trueL);
        }
        return str;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        previous = (ImageView) findViewById(R.id.previous);
        playpause = (ImageView) findViewById(R.id.play);
        next = (ImageView) findViewById(R.id.next);
        play = getResources().getDrawable(R.drawable.play);
        pause = getResources().getDrawable(R.drawable.pause);
        an = (TextView) findViewById(R.id.artistname);
        tn = (TextView) findViewById(R.id.trackname);

        previous.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //buttonaction
                player.seekToPreviousMediaItem();
            }
        });
        playpause.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //buttonaction
                if(player.isPlaying()) {
                    player.pause();
                } else {
                    player.play();
                }
            }
        });
        next.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //buttonaction
                player.seekToNextMediaItem();
            }
        });
        if(Build.VERSION.SDK_INT >=Build.VERSION_CODES.O){

            NotificationChannel channel= new NotificationChannel("playback","Video Playback", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager =getSystemService(NotificationManager.class);
            channel.setSound(null, null);
            manager.createNotificationChannel(channel);
        }
        mediaSession = new MediaSessionCompat(this, getPackageName());
        mediaSessionConnector = new MediaSessionConnector(mediaSession);
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setUsage(C.USAGE_MEDIA)
                .setContentType(C.AUDIO_CONTENT_TYPE_MOVIE)
                .build();
        // create Wifi and wake locks
        mWifiLock = ((WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE)).createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "S3Manager:wifi_lock");
        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "S3Manager:wake_lock");
        //Get media url
        videoURL = getIntent().getStringExtra("video_url");
        title = getIntent().getStringExtra("title");
        videocache = getIntent().getIntExtra("videocache", 40);
        buffersize = getIntent().getIntExtra("buffersize", 2000);
        isplaylist = getIntent().getBooleanExtra("isplaylist", false);
        queue = getIntent().getStringArrayListExtra("queue");
        names = getIntent().getStringArrayListExtra("names");
        getSupportActionBar().setTitle(title);
        loadControl = new DefaultLoadControl.Builder().setBufferDurationsMs(2000, buffersize, 1500, 2000).build();

        @DefaultRenderersFactory.ExtensionRendererMode int extensionRendererMode = DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER;

        renderersFactory = new DefaultRenderersFactory(this) .setExtensionRendererMode(extensionRendererMode);
        standaloneDatabaseProvider = new StandaloneDatabaseProvider(this);
        maxCacheSize = (long)videocache * 1024 * 1024;
        playerView = findViewById(R.id.player_view);
        controlView = findViewById(R.id.control_view);
        // creating a variable for exoplayer
        player = new ExoPlayer.Builder(this).setLoadControl(loadControl).build();
        player.setAudioAttributes(audioAttributes, true);
        mediaSessionConnector.setPlayer(player);
        //mediaSession.setActive(true);
        evictor = new LeastRecentlyUsedCacheEvictor(maxCacheSize);
        if(SdcardCheck.isInstalledOnSdCard(this)) {
            simpleCache = new SimpleCache(
                    new File(this.getExternalFilesDirs(null)[this.getExternalFilesDirs(null).length-1], "media"),
                    evictor,
                    standaloneDatabaseProvider);
        } else {
            simpleCache = new SimpleCache(
                    new File(this.getFilesDir(), "media"),
                    evictor,
                    standaloneDatabaseProvider);
        }
        playerView.setPlayer(player);
        controlView.setPlayer(player);
        playerView.setUseController(false);
        //MediaItem mediaItem = MediaItem.fromUri(videoURL);

        // Set the media item to be played.
        //player.setMediaItem(mediaItem);
        // Prepare the player.
        player.setPlayWhenReady(true);
        playerNotificationManager = new PlayerNotificationManager.Builder(this, notificationId, "playback").build();
        playerNotificationManager.setUseNextActionInCompactView(true);
        playerNotificationManager.setUsePreviousActionInCompactView(true);
        playerNotificationManager.setMediaSessionToken(mediaSession.getSessionToken());
        playerNotificationManager.setPlayer(player);
        if (isplaylist) {
            for (int i = 0; i < queue.size(); i++) {
                MediaItem mediaItem = MediaItem.fromUri(URLify(queue.get(i)));
                player.addMediaItem(mediaItem);
            }
            getSupportActionBar().setTitle(names.get(player.getCurrentMediaItemIndex()));
            player.prepare();
            try {
                player.seekTo(names.indexOf(title), 0);
            }catch (Exception e) {
                System.out.println("Playing m3u file");
            }
        } else {
            MediaItem mediaItem = MediaItem.fromUri(URLify(videoURL));
            player.setMediaItem(mediaItem);
            player.prepare();
        }
        // Start the playback.
        player.play();

        player.addListener(new Player.Listener() {
            @Override

            public void onPlayerError(PlaybackException error) {
                Throwable cause = error.getCause();
                if(success) {
                    player.pause();
                } else {
                    // An HTTP error occurred.
                    //System.out.println("Playback error F");
                    Toast.makeText(getApplicationContext(), Objects.requireNonNull(error.getCause()).toString(), Toast.LENGTH_SHORT).show();
                    player.release();
                    finish();
                }
            }

            @Override
            public void onIsPlayingChanged(boolean isPlaying) {
                if(player.isPlaying()) {
                    playpause.setImageDrawable(pause);
                    playpause.setContentDescription(getResources().getString(R.string.pause));
                } else {
                    playpause.setImageDrawable(play);
                    playpause.setContentDescription(getResources().getString(R.string.play));
                }
                Player.Listener.super.onIsPlayingChanged(isPlaying);
            }

            @Override
            public void onTracksChanged(Tracks tracks) {
                if(isplaylist) {
                    getSupportActionBar().setTitle(names.get(player.getCurrentMediaItemIndex()));
                }
                Player.Listener.super.onTracksChanged(tracks);
            }

            public void onMediaMetadataChanged(MediaMetadata mediaMetadata) {
                //Station 5 does not display metadata
                trackname = (String) mediaMetadata.title;
                tn.setText(trackname);
                artistname = (String) mediaMetadata.artist;
                an.setText(artistname);
                //System.out.println(trackname);
            }
        });

        player.addListener(new Player.Listener() {
            @Override
            public void onPlaybackStateChanged(@Player.State int state) {
                if (state == 3) {
                    // Active playback.
                    mediaSession.setActive(true);
                    success = true;
                    //Acquiring WakeLock and WifiLock if not held
                    if (!mWifiLock.isHeld()) {
                        mWifiLock.acquire();
                        //System.out.println("WifiLock acquired");
                    }
                    if (!mWakeLock.isHeld()) {
                        mWakeLock.acquire();
                        //System.out.println("WakeLock acquired");
                    }
                } else if (state == 2) {
                    // Buffering.
                    //Acquiring WakeLock and WifiLock if not held
                    if (!mWifiLock.isHeld()) {
                        mWifiLock.acquire();
                        //System.out.println("WifiLock acquired");
                    }
                    if (!mWakeLock.isHeld()) {
                        mWakeLock.acquire();
                        //System.out.println("WakeLock acquired");
                    }
                } else {
                    //Player inactive
                    mediaSession.setActive(false);
                    //Releasing WifiLock and WakeLock if held
                    if (mWifiLock.isHeld()) {
                        mWifiLock.release();
                        //System.out.println("WifiLock released");
                    }
                    if (mWakeLock.isHeld()) {
                        mWakeLock.release();
                        //System.out.println("WakeLock released");
                    }
                    // Not playing because playback is paused, ended, suppressed, or the player
                    // is buffering, stopped or failed. Check player.getPlayWhenReady,
                    // player.getPlaybackState, player.getPlaybackSuppressionReason and
                    // player.getPlaybackError for details.
                }
            }
        });
    }

    @Override

    public void onDestroy() {
        if (mWifiLock.isHeld()) {
            mWifiLock.release();
            //System.out.println("WifiLock acquired");
        }
        if (mWakeLock.isHeld()) {
            mWakeLock.release();
            //System.out.println("WakeLock acquired");
        }
        mediaSessionConnector.setPlayer(null);
        //deleteCache(this, standaloneDatabaseProvider);
        mediaSession.setActive(false);
        playerNotificationManager.setPlayer(null);
        player.release();
        playerView.setPlayer(null);
        simpleCache.release();
        standaloneDatabaseProvider.close();
        super.onDestroy();
    }

    protected void onNewIntent(Intent intent) {
        videoURL = intent.getStringExtra("video_url");
        title = intent.getStringExtra("title");
        videocache = intent.getIntExtra("videocache", 40);
        buffersize = intent.getIntExtra("buffersize", 2000);
        isplaylist = intent.getBooleanExtra("isplaylist", false);
        queue = intent.getStringArrayListExtra("queue");
        names = intent.getStringArrayListExtra("names");
        getSupportActionBar().setTitle(title);
        if (isplaylist) {
            player.clearMediaItems();
            for (int i = 0; i < queue.size(); i++) {
                MediaItem mediaItem = MediaItem.fromUri(queue.get(i));
                player.addMediaItem(mediaItem);
            }
            player.prepare();
            player.seekTo(names.indexOf(title), 0);
        } else {
            MediaItem mediaItem = MediaItem.fromUri(videoURL);
            player.setMediaItem(mediaItem);
            player.prepare();
        }
        // Start the playback.
        player.play();
        super.onNewIntent(intent);
    }

    /*static void deleteCache(Context context, StandaloneDatabaseProvider database) {
        SimpleCache.delete(new File(context.getCacheDir(), "media"), database);
    }*/

}