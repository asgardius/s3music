# Asgardius S3 Music

Play your music stored on servers that are compatible with S3 Object Storage Protocol

Current feature list

* Audio and Video playback (opus, ogg, oga, mp3, m4a, flac, mka, mkv, mp4, m4v, webm)
* Folder based playlists

Planned feature list

* Nothing for now

This app is based on Asgardius S3 Manager

This app is a work in progress, so it have some bugs that need to be fixed

[<img src="app-store-badges/play-store.png"
    alt="Get it on Google Play"
    height="80">](https://play.google.com/store/apps/details?id=asgardius.page.s3musicmk2)

F-droid release may take a few days to get updated [More info here](https://www.f-droid.org/en/docs/FAQ_-_App_Developers/#ive-published-a-new-release-why-is-it-not-in-the-repository)

[Steps to joining to Google Play Alpha testing channel are available here](https://forum.asgardius.company/d/1-asgardius-s3-manager-testing)

You can get help at https://forum.asgardius.company/t/s3-manager

You can find app documentation at https://wiki-en.asgardius.company/index.php?title=Asgardius_S3_Manager_Documentation (english) or https://wiki-es.asgardius.company/index.php?title=Documentacion_Asgardius_S3_Manager (spanish)

Supported languages

* English
* Spanish

Knnown issues

* Object listing can be slow on buckets with a lot of objects (4000+)
* Slow user interface on some low-end devices
* Running screen restarts after toggling system dark mode

Known supported providers

* Amazon Web Services
* Scaleway Elements
* Wasabi Cloud
* Backblaze B2
* Cloudflare R2
* MinIO
* Garage

Known not supported providers

* Google Cloud (Not compatible with S3v4)
* Oracle Cloud (compatibility issues with S3v4)
